﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using VulkanCore;

using static CryogenicEngine.CryoContent.Loader;

namespace CryogenicEngine.CryoContent
{
    public class CryoContentLoader
    {
        CryoApp cryoApp;

        private readonly string _contentRoot;
        private readonly Dictionary<string, IDisposable> _cachedContent = new Dictionary<string, IDisposable>();



        public CryoContentLoader(CryoApp app,string contentRoot)
        {
            cryoApp = app;
            _contentRoot = contentRoot;
        }

        public T Load<T>(string contentName)
        {
            if (_cachedContent.TryGetValue(contentName, out IDisposable value))
                return (T)value;

            string path = Path.Combine(_contentRoot, contentName);
            string extension = Path.GetExtension(path);

            Type type = typeof(T);

            if (type == typeof(ShaderModule))
            {
                value = LoadShaderModule(cryoApp.cryoVulkan, path);
            }
            else if (type == typeof(CryoWorld.CryoMap))
            {
                value = MapLoader(path);
            }
            else if (type == typeof(CryoVulkan.CryoVulkanImage))
            {
                if (extension.Equals(".ktx", StringComparison.OrdinalIgnoreCase))
                {
                    value = LoadKtxVulkanImage(path);
                }
            }
            if (value == null)
                throw new NotImplementedException("Content type or extension not implemented.");

            _cachedContent.Add(contentName, value);
            return (T)value;
        }
    }

    internal static partial class Loader
    {
        public static ShaderModule LoadShaderModule(CryoVulkan.CryoVulkan ctx, string path)
        {
            const int defaultBufferSize = 4096;
            using (FileStream stream = new FileStream(path, FileMode.Open))
            using (var ms = new MemoryStream())
            {
                stream.CopyTo(ms, defaultBufferSize);
                return ctx.Device.CreateShaderModule(new ShaderModuleCreateInfo(ms.ToArray()));
            }
        }
    }
}
