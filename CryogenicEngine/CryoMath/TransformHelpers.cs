﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Numerics;

namespace CryogenicEngine.CryoMath
{
    internal class TransformHelpers
    {
        public static Matrix4x4 Rotate(float angle, Vector3 v)
        {
            var c = MathF.Cos(angle);
            var s = MathF.Sin(angle);

            var axis = Vector3.Normalize(v);
            var temp = (1 - c) * axis;

            var m = Matrix4x4.Identity;
            m.M11 = c + temp.X * axis.X;
            m.M21 = 0 + temp.X * axis.Y + s * axis.Z;
            m.M31 = 0 + temp.X * axis.Z - s * axis.Y;

            m.M12 = 0 + temp.Y * axis.X - s * axis.Z;
            m.M22 = c + temp.Y * axis.Y;
            m.M32 = 0 + temp.Y * axis.Z + s * axis.X;

            m.M13 = 0 + temp.Z * axis.X + s * axis.Y;
            m.M23 = 0 + temp.Z * axis.Y - s * axis.X;
            m.M33 = c + temp.Z * axis.Z;
            return m;
        }

        public static Matrix4x4 LookAt(Vector3 eye, Vector3 center, Vector3 up)
        {
            var f = Vector3.Normalize((center - eye));
            var s = Vector3.Normalize(Vector3.Cross(f, up));
            var u = Vector3.Cross(s, f);
            var m = Matrix4x4.Identity;

            //Old : Culomn  , Row       0-3
            //New : Row     , Culomn    1-4

            m.M11 = s.X;
            m.M12 = s.Y;
            m.M13 = s.Z;

            m.M21 = u.X;
            m.M22 = u.Y;
            m.M23 = u.Z;

            m.M31 = -f.X;
            m.M32 = -f.Y;
            m.M33 = -f.Z;

            m.M41 = -Vector3.Dot(s, eye);
            m.M42 = -Vector3.Dot(u, eye);
            m.M43 = Vector3.Dot(f, eye);
            return m;
        }

        public static Matrix4x4 PreparePerspectiveProjectionMatrix(float aspect_ratio,
                                                float field_of_view,
                                                float near_plane,
                                                float far_plane)
        {
            float f = 1.0f / MathF.Tan(Radian(0.5f * field_of_view));

            Matrix4x4 perspective_projection_matrix = new Matrix4x4(
                  f / aspect_ratio,
                  0.0f,
                  0.0f,
                  0.0f,

                  0.0f,
                  -f,
                  0.0f,
                  0.0f,

                  0.0f,
                  0.0f,
                  far_plane / (near_plane - far_plane),
                  -1.0f,

                  0.0f,
                  0.0f,
                  (near_plane * far_plane) / (near_plane - far_plane),
                  0.0f
            );

            return perspective_projection_matrix;
        }


        public static float Radian(float degrees)
        {
            const float a = MathF.PI / 180;
            return degrees * a;
        }
    }

    public static class Vector3Extentons
    {
        public static Vector3 Rotate(this Vector3 v, Quaternion q)
        {
            v = Vector3.Transform(v, q);
            v = Vector3.Normalize(v);
            //Console.WriteLine(v);
            return v;
        }
    }
}
