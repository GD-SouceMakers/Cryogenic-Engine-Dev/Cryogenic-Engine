﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using VulkanCore;
using VulkanCore.Khr;

using static CryogenicEngine.CryoMath.TransformHelpers;

namespace CryogenicEngine.CryoRender
{

    //class BaseRenderer
    //{
    //    CryoApp cryoApp;
    //    CryoVulkan.CryoVulkan cryoVulkan;

    //    private readonly Stack<IDisposable> _toDisposeFrame = new Stack<IDisposable>();

    //    Sampler sampler;
    //    CryoVulkan.CryoVulkanImage testTexture;

    //    CryoVulkan.CryoVulkanImage depthImage;

    //    SwapchainKhr swapchain;
    //    Image[] swapchainImages;

    //    ImageView[] _imageViews;
    //    Framebuffer[] _framebuffers;

    //    internal RenderPass renderPass;

    //     DescriptorSetLayout descriptorSetLayout;
    //    internal DescriptorPool descriptorPool;
    //    DescriptorSet descriptorSet;

    //    PipelineLayout pipelineLayout;
    //    Pipeline pipeline;

    //    CommandBuffer[] commandBuffers;

    //    CryoVulkan.CryoVulkanBuffer vertexBuffer;
    //    CryoVulkan.CryoVulkanBuffer indexBuffer;

    //    CryoVulkan.CryoVulkanBuffer uniformBuffer;

    //    //Semaphores
    //    Semaphore ImageAvailableSemaphore;
    //    Semaphore RenderingFinishedSemaphore;

    //    Vertex[] vertices =
    //        {
    //            /*
    //            {{-0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}},
    //            {{0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}},
    //            {{0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}},
    //            {{-0.5f, 0.5f}, {1.0f, 1.0f, 1.0f}}
    //            */
    //            new Vertex(new Vector3(-0.5f, -0.5f,0f), new Vector3( 1.0f, 0.0f, 0.0f ), new Vector2(1.0f,0f)),
    //            new Vertex(new Vector3(0.5f, -0.5f,0f),  new Vector3( 0.0f, 1.0f, 0.0f), new Vector2(0.0f,0f)),
    //            new Vertex(new Vector3(0.5f, 0.5f,0f),  new Vector3( 0.0f, 0.0f, 1.0f), new Vector2(0.0f,1.0f)),
    //            new Vertex(new Vector3(-0.5f, 0.5f,0f),  new Vector3( 1.0f, 1.0f, 1.0f), new Vector2(1.0f,1.0f)),

    //            new Vertex(new Vector3(-0.5f, -0.5f,1f), new Vector3( 1.0f, 0.0f, 0.0f ), new Vector2(1.0f,0f)),
    //            new Vertex(new Vector3(0.5f, -0.5f,1f),  new Vector3( 0.0f, 1.0f, 0.0f), new Vector2(0.0f,0f)),
    //            new Vertex(new Vector3(0.5f, 0.5f,1f),  new Vector3( 0.0f, 0.0f, 1.0f), new Vector2(0.0f,1.0f)),
    //            new Vertex(new Vector3(-0.5f, 0.5f,1f),  new Vector3( 1.0f, 1.0f, 1.0f), new Vector2(1.0f,1.0f))
    //        };

    //    int[] indices = {
    //        0, 1, 2, 2, 3, 0,
    //        4, 5, 6, 6, 7, 4
    //    };

    //    public static BaseRenderer Instance;

    //    public BaseRenderer(CryogenicEngine.CryoApp app, CryoVulkan.CryoVulkan vulkan)
    //    {
    //        Instance = this;

    //        cryoApp = app;

    //        cryoVulkan = vulkan;

    //        testTexture = app.contentLoader.Load<CryoVulkan.CryoVulkanImage>("Textures/IndustryForgedDark512.ktx");

    //        ImageAvailableSemaphore = cryoVulkan.Device.CreateSemaphore();
    //        RenderingFinishedSemaphore = cryoVulkan.Device.CreateSemaphore();

    //        createTextureSampler();
    //        createDepthResources();

    //        //Setup swapchain
    //        CreateSwapchain();

    //        CreateImageViews();

    //        CreateRenderPass();

    //        CreateDescriptorSetLayout();

    //        CreatePipelineLayout();
    //        CreateGraphicsPipeline();

    //        CreateFramebuffers();

    //        CreateVertexBuffer();
    //        CreateIndexBuffer();
    //        CreateUniformBuffer();

    //        CreateDescriptorPool();
    //        CreateDescriptorSet();


    //        CreateCommandBuffers();
    //        RecordCommandBuffers();
    //    }

    //    void createTextureSampler()
    //    {

    //        var createInfo = new SamplerCreateInfo
    //        {
    //            MagFilter = Filter.Linear,
    //            MinFilter = Filter.Linear,
    //            MipmapMode = SamplerMipmapMode.Linear
    //        };
    //        // We also enable anisotropic filtering. Because that feature is optional, it must be
    //        // checked if it is supported by the device.
    //        if (cryoVulkan.Features.SamplerAnisotropy)
    //        {
    //            createInfo.AnisotropyEnable = true;
    //            createInfo.MaxAnisotropy = cryoVulkan.Properties.Limits.MaxSamplerAnisotropy;
    //        }
    //        else
    //        {
    //            createInfo.MaxAnisotropy = 1.0f;
    //        }
    //        sampler = cryoVulkan.Device.CreateSampler(createInfo);
    //    }

    //    void createDepthResources()
    //    {
    //        depthImage = CryoVulkan.CryoVulkanImage.DepthStencil(cryoApp.Host.window.Width, cryoApp.Host.window.Height);
    //    }

    //    void recreateSwapChain()
    //    {
    //        cryoVulkan.Device.WaitIdle();

    //        // Dispose all frame dependent resources.
    //        while (_toDisposeFrame.Count > 0)
    //            _toDisposeFrame.Pop().Dispose();

    //        // Reset all the command buffers allocated from the pools.
    //        cryoVulkan.GraphicsCommandPool.Reset();
    //        cryoVulkan.ComputeCommandPool.Reset();

    //        CreateSwapchain();
    //        CreateImageViews();
    //        CreateRenderPass();
    //        CreateGraphicsPipeline();
    //        CreateFramebuffers();
    //        RecordCommandBuffers();
    //    }

    //    private void CreateSwapchain()
    //    {
    //        SurfaceCapabilitiesKhr capabilities = cryoVulkan.PhysicalDevice.GetSurfaceCapabilitiesKhr(cryoVulkan.surface);
    //        SurfaceFormatKhr[] formats = cryoVulkan.PhysicalDevice.GetSurfaceFormatsKhr(cryoVulkan.surface);
    //        PresentModeKhr[] presentModes = cryoVulkan.PhysicalDevice.GetSurfacePresentModesKhr(cryoVulkan.surface);
    //        Format format = formats.Length == 1 && formats[0].Format == Format.Undefined
    //            ? Format.B8G8R8A8UNorm
    //            : formats[0].Format;
    //        PresentModeKhr presentMode =
    //            presentModes.Contains(PresentModeKhr.Mailbox) ? PresentModeKhr.Mailbox :
    //            presentModes.Contains(PresentModeKhr.FifoRelaxed) ? PresentModeKhr.FifoRelaxed :
    //            presentModes.Contains(PresentModeKhr.Fifo) ? PresentModeKhr.Fifo :
    //            PresentModeKhr.Immediate;

    //        swapchain = cryoVulkan.Device.CreateSwapchainKhr(new SwapchainCreateInfoKhr(
    //            cryoVulkan.surface,
    //            format,
    //            capabilities.CurrentExtent,
    //            capabilities.CurrentTransform,
    //            presentMode));

    //        swapchainImages = swapchain.GetImages();
    //        ToDispose(swapchainImages);
    //    }

    //    private void CreateImageViews()
    //    {
    //        var imageViews = new ImageView[swapchainImages.Length];
    //        for (int i = 0; i < swapchainImages.Length; i++)
    //        {
    //            imageViews[i] = swapchainImages[i].CreateView(new ImageViewCreateInfo(
    //                swapchain.Format,
    //                new ImageSubresourceRange(ImageAspects.Color, 0, 1, 0, 1)));
    //        }
    //        _imageViews = imageViews;
    //        ToDispose(_imageViews);
    //    }

    //    private void CreateRenderPass()
    //    {
    //        var subpasses = new[]
    //        {
    //            new SubpassDescription(new[] {
    //                new AttachmentReference(0, ImageLayout.ColorAttachmentOptimal)                    
    //            },
    //            new AttachmentReference(1, ImageLayout.DepthStencilAttachmentOptimal))
    //        };
    //        var attachments = new[]
    //        {
    //            new AttachmentDescription
    //            {
    //                Samples = SampleCounts.Count1,
    //                Format = swapchain.Format,
    //                InitialLayout = ImageLayout.Undefined,
    //                FinalLayout = ImageLayout.PresentSrcKhr,
    //                LoadOp = AttachmentLoadOp.Clear,
    //                StoreOp = AttachmentStoreOp.Store,
    //                StencilLoadOp = AttachmentLoadOp.DontCare,
    //                StencilStoreOp = AttachmentStoreOp.DontCare
    //            },

    //            // Depth attachment.
    //            new AttachmentDescription
    //            {
    //                Format = depthImage.Format,
    //                Samples = SampleCounts.Count1,
    //                LoadOp = AttachmentLoadOp.Clear,
    //                StoreOp = AttachmentStoreOp.DontCare,
    //                StencilLoadOp = AttachmentLoadOp.DontCare,
    //                StencilStoreOp = AttachmentStoreOp.DontCare,
    //                InitialLayout = ImageLayout.Undefined,
    //                FinalLayout = ImageLayout.DepthStencilAttachmentOptimal
    //            }
    //        };
    //        SubpassDependency dependency = new SubpassDependency
    //        {
    //            SrcSubpass = Constant.SubpassExternal,
    //            DstSubpass = 0,
    //            SrcStageMask = PipelineStages.ColorAttachmentOutput,
    //            SrcAccessMask = 0,
    //            DstStageMask = PipelineStages.ColorAttachmentOutput,
    //            DstAccessMask = Accesses.ColorAttachmentRead | Accesses.ColorAttachmentWrite,
    //        };

    //        var createInfo = new RenderPassCreateInfo(subpasses,
    //            attachments,
    //            dependencies: new[] { dependency });
    //        renderPass = cryoVulkan.Device.CreateRenderPass(createInfo);

    //        ToDispose(renderPass);
    //    }

    //    void CreateDescriptorSetLayout()
    //    {
    //        //TODO add desposing

    //        descriptorSetLayout = cryoVulkan.Device.CreateDescriptorSetLayout(new DescriptorSetLayoutCreateInfo(
    //            new DescriptorSetLayoutBinding(0, DescriptorType.UniformBuffer, 1, ShaderStages.Vertex),
    //            new DescriptorSetLayoutBinding(1, DescriptorType.CombinedImageSampler, 1, ShaderStages.Fragment)));
    //    }

    //    private void CreatePipelineLayout()
    //    {
    //        var layoutCreateInfo = new PipelineLayoutCreateInfo(new[] { descriptorSetLayout });
    //        pipelineLayout = cryoVulkan.Device.CreatePipelineLayout(layoutCreateInfo);
    //    }

    //    private void CreateGraphicsPipeline()
    //    {
    //        var vertexShader = CryoContent.Loader.LoadShaderModule(cryoVulkan, "Content/Shader/shader.vert.spv");
    //        var fragmentShader = CryoContent.Loader.LoadShaderModule(cryoVulkan, "Content/Shader/shader.frag.spv");
    //        var shaderStageCreateInfos = new[]
    //        {
    //            new PipelineShaderStageCreateInfo(ShaderStages.Vertex, vertexShader, "main"),
    //            new PipelineShaderStageCreateInfo(ShaderStages.Fragment, fragmentShader, "main")
    //        };

    //        var vertexInputStateCreateInfo = new PipelineVertexInputStateCreateInfo(
    //            Vertex.getVertexInputBindingDescription(),
    //            Vertex.getVertexInputAttributeDescription()
    //        );

    //        var depthStencilCreateInfo = new PipelineDepthStencilStateCreateInfo
    //        {
    //            DepthTestEnable = true,
    //            DepthWriteEnable = true,
    //            DepthCompareOp = CompareOp.LessOrEqual,
    //            Back = new StencilOpState
    //            {
    //                FailOp = StencilOp.Keep,
    //                PassOp = StencilOp.Keep,
    //                CompareOp = CompareOp.Always
    //            },
    //            Front = new StencilOpState
    //            {
    //                FailOp = StencilOp.Keep,
    //                PassOp = StencilOp.Keep,
    //                CompareOp = CompareOp.Always
    //            }
    //        };

    //        var inputAssemblyStateCreateInfo = new PipelineInputAssemblyStateCreateInfo(PrimitiveTopology.TriangleList);

    //        var viewportStateCreateInfo = new PipelineViewportStateCreateInfo(
    //            new Viewport(0, 0, cryoApp.Host.window.Width, cryoApp.Host.window.Height),
    //            new Rect2D(0, 0, cryoApp.Host.window.Width, cryoApp.Host.window.Height));

    //        var rasterizationStateCreateInfo = new PipelineRasterizationStateCreateInfo
    //        {
    //            PolygonMode = PolygonMode.Fill,
    //            CullMode = CullModes.Back,
    //            FrontFace = FrontFace.CounterClockwise,
    //            LineWidth = 1.0f
    //        };

    //        var multisampleStateCreateInfo = new PipelineMultisampleStateCreateInfo
    //        {
    //            RasterizationSamples = SampleCounts.Count1,
    //            MinSampleShading = 1.0f
    //        };

    //        var colorBlendAttachmentState = new PipelineColorBlendAttachmentState
    //        {
    //            SrcColorBlendFactor = BlendFactor.One,
    //            DstColorBlendFactor = BlendFactor.Zero,
    //            ColorBlendOp = BlendOp.Add,
    //            SrcAlphaBlendFactor = BlendFactor.One,
    //            DstAlphaBlendFactor = BlendFactor.Zero,
    //            AlphaBlendOp = BlendOp.Add,
    //            ColorWriteMask = ColorComponents.All
    //        };
    //        var colorBlendStateCreateInfo = new PipelineColorBlendStateCreateInfo(
    //            new[] { colorBlendAttachmentState });

    //        var pipelineCreateInfo = new GraphicsPipelineCreateInfo(
    //            pipelineLayout, renderPass, 0,
    //            shaderStageCreateInfos,
    //            inputAssemblyStateCreateInfo,
    //            vertexInputStateCreateInfo,
    //            rasterizationStateCreateInfo,
    //            viewportState: viewportStateCreateInfo,
    //            multisampleState: multisampleStateCreateInfo,
    //            colorBlendState: colorBlendStateCreateInfo,
    //            depthStencilState: depthStencilCreateInfo);
    //        pipeline = cryoVulkan.Device.CreateGraphicsPipeline(pipelineCreateInfo);

    //        ToDispose(pipeline);
    //    }

    //    private void CreateFramebuffers()
    //    {
    //        var framebuffers = new Framebuffer[swapchainImages.Length];
    //        for (int i = 0; i < swapchainImages.Length; i++)
    //        {
    //            framebuffers[i] = renderPass.CreateFramebuffer(new FramebufferCreateInfo(
    //                new[] { _imageViews[i] , depthImage.View },
    //                cryoApp.Host.window.Width,
    //                cryoApp.Host.window.Height));
    //        }
    //        _framebuffers = framebuffers;

    //        ToDispose(_framebuffers);
    //    }

    //    private void CreateVertexBuffer()
    //    {
    //        //TODO add desposing

    //        long size = vertices.Length * Interop.SizeOf<Vertex>();

    //        var stagingBuffer = createBuffer(size, BufferUsages.TransferSrc, MemoryProperties.HostVisible | MemoryProperties.HostCoherent);

    //        IntPtr vertexPtr = stagingBuffer.Memory.Map(0, stagingBuffer.Size);
    //        Interop.Write(vertexPtr, vertices);
    //        stagingBuffer.Memory.Unmap();

    //        vertexBuffer = createBuffer(size, BufferUsages.TransferDst | BufferUsages.VertexBuffer, MemoryProperties.DeviceLocal);

    //        copyBuffer(stagingBuffer, vertexBuffer, stagingBuffer.Size);

    //        stagingBuffer.Dispose();
    //    }

    //    private void CreateIndexBuffer()
    //    {
    //        long size = indices.Length * Interop.SizeOf<long>();

    //        var stagingBuffer = createBuffer(size, BufferUsages.TransferSrc, MemoryProperties.HostVisible | MemoryProperties.HostCoherent);

    //        IntPtr indexPtr = stagingBuffer.Memory.Map(0, stagingBuffer.Size);
    //        Interop.Write(indexPtr, indices);
    //        stagingBuffer.Memory.Unmap();

    //        indexBuffer = createBuffer(size, BufferUsages.TransferDst | BufferUsages.IndexBuffer, MemoryProperties.DeviceLocal);

    //        copyBuffer(stagingBuffer, indexBuffer, size);

    //        stagingBuffer.Dispose();
    //    }

    //    private void CreateUniformBuffer()
    //    {
    //        //TODO add despose
    //        long bufferSize = Marshal.SizeOf<UniformBufferObject>();
    //        uniformBuffer = createBuffer(bufferSize, BufferUsages.UniformBuffer, MemoryProperties.HostVisible | MemoryProperties.HostCoherent);
    //    }

    //    CryoVulkan.CryoVulkanBuffer createBuffer(long size, BufferUsages bufferUsages, MemoryProperties properties)
    //    {
    //        //long size = vertices.Length * Interop.SizeOf<Vertex>();

    //        BufferCreateInfo bufferCreateInfo = new BufferCreateInfo();
    //        bufferCreateInfo.Size = size;
    //        bufferCreateInfo.Usage = bufferUsages;
    //        bufferCreateInfo.SharingMode = SharingMode.Exclusive;

    //        //TODO Error check
    //        var buffer = cryoVulkan.Device.CreateBuffer(bufferCreateInfo);

    //        MemoryRequirements stagingReq = buffer.GetMemoryRequirements();
    //        int vertexMemoryTypeIndex = cryoVulkan.MemoryProperties.MemoryTypes.IndexOf(
    //            stagingReq.MemoryTypeBits,
    //            properties);

    //        //TODO Error check
    //        DeviceMemory memory = cryoVulkan.Device.AllocateMemory(new MemoryAllocateInfo(stagingReq.Size, vertexMemoryTypeIndex));

    //        buffer.BindMemory(memory);

    //        return new CryoVulkan.CryoVulkanBuffer(buffer, memory, size);
    //    }

    //    void copyBuffer(CryoVulkan.CryoVulkanBuffer srcBuffer, CryoVulkan.CryoVulkanBuffer dstBuffer, long size)
    //    {
    //        CommandBufferAllocateInfo allocInfo = new CommandBufferAllocateInfo();
    //        allocInfo.Level = CommandBufferLevel.Primary;
    //        allocInfo.CommandBufferCount = 1;

    //        CommandBuffer commandBuffer = cryoVulkan.GraphicsCommandPool.AllocateBuffers(allocInfo)[0];

    //        commandBuffer.Begin(new CommandBufferBeginInfo(CommandBufferUsages.OneTimeSubmit));
    //        commandBuffer.CmdCopyBuffer(srcBuffer, dstBuffer, new BufferCopy(size));
    //        commandBuffer.End();

    //        Fence fence = cryoVulkan.Device.CreateFence();
    //        cryoVulkan.GraphicsQueue.Submit(new SubmitInfo(commandBuffers: new[] { commandBuffer }), fence);
    //        fence.Wait();

    //        fence.Dispose();
    //        commandBuffer.Dispose();

    //        return;
    //    }

    //    private void CreateDescriptorPool()
    //    {
    //        DescriptorPoolSize[] poolSizes = new[] {
    //            new DescriptorPoolSize(DescriptorType.UniformBuffer, 1),
    //            new DescriptorPoolSize(DescriptorType.CombinedImageSampler, 1)
    //        };

    //        DescriptorPoolCreateInfo poolCreateInfo = new DescriptorPoolCreateInfo(1, poolSizes);

    //        //TODO error check
    //        //TODO desposing
    //        descriptorPool = cryoVulkan.Device.CreateDescriptorPool(poolCreateInfo);
    //    }

    //    private void CreateDescriptorSet()
    //    {
    //        DescriptorSetAllocateInfo allocateInfo = new DescriptorSetAllocateInfo(1, new[] { descriptorSetLayout });
    //        //TODO error check
    //        descriptorSet = descriptorPool.AllocateSets(allocateInfo)[0];

    //        //DescriptorBufferInfo bufferInfo = new DescriptorBufferInfo(uniformBuffer, 0, Marshal.SizeOf<UniformBufferObject>());

    //        WriteDescriptorSet[] descriptorWriters = new[] {
    //            new WriteDescriptorSet(descriptorSet, 0, 0, 1, DescriptorType.UniformBuffer, 
    //                bufferInfo: new[] { new DescriptorBufferInfo(uniformBuffer) }),
    //            new WriteDescriptorSet(descriptorSet, 1, 0, 1, DescriptorType.CombinedImageSampler,
    //                imageInfo: new[] { new DescriptorImageInfo(sampler, testTexture.View, ImageLayout.General) })
    //        };

    //        descriptorPool.UpdateSets(descriptorWriters);
    //    }

    //    private void CreateCommandBuffers()
    //    {
    //        commandBuffers = cryoVulkan.GraphicsCommandPool.AllocateBuffers(
    //            new CommandBufferAllocateInfo(CommandBufferLevel.Primary, swapchainImages.Length));
    //    }

    //    private void RecordCommandBuffers()
    //    {
    //        var subresourceRange = new ImageSubresourceRange(ImageAspects.Color, 0, 1, 0, 1);
    //        for (int i = 0; i < commandBuffers.Length; i++)
    //        {
    //            CommandBuffer cmdBuffer = commandBuffers[i];
    //            cmdBuffer.Begin(new CommandBufferBeginInfo(CommandBufferUsages.SimultaneousUse));

    //            var renderPassBeginInfo = new RenderPassBeginInfo(
    //            _framebuffers[i],
    //            new Rect2D(Offset2D.Zero, new Extent2D(cryoApp.Host.window.Width, cryoApp.Host.window.Height)),
    //            new ClearColorValue(new ColorF4(0.39f, 0.58f, 0.93f, 1.0f)),
    //            new ClearDepthStencilValue(1.0f,0)
    //            );

    //            cmdBuffer.CmdBeginRenderPass(renderPassBeginInfo);

    //            /*
    //            cmdBuffer.CmdBindPipeline(PipelineBindPoint.Graphics, pipeline);

    //            cmdBuffer.CmdBindVertexBuffer(vertexBuffer);
    //            cmdBuffer.CmdBindIndexBuffer(indexBuffer);
    //            cmdBuffer.CmdBindDescriptorSet(PipelineBindPoint.Graphics, pipelineLayout, descriptorSet);

    //            cmdBuffer.CmdDrawIndexed(indices.Length);
    //            */

    //            //CryoRenderer.Instance.drawCalls(cmdBuffer);

    //            cmdBuffer.CmdEndRenderPass();

    //            cmdBuffer.End();
    //        }
    //    }

    //    public void Draw(Timer timer)
    //    {
    //        UpdateUniformBuffer(timer.TotalTime);
    //        DrawFrame();
    //    }

    //    private void DrawFrame()
    //    {
    //        // Acquire an index of drawing image for this frame.
    //        int imageIndex = swapchain.AcquireNextImage(semaphore: ImageAvailableSemaphore);

    //        // Submit recorded commands to graphics queue for execution.
    //        cryoVulkan.GraphicsQueue.Submit(
    //            ImageAvailableSemaphore,
    //            PipelineStages.Transfer,
    //            commandBuffers[imageIndex],
    //            RenderingFinishedSemaphore
    //        );

    //        // Present the color output to screen.
    //        cryoVulkan.PresentQueue.PresentKhr(RenderingFinishedSemaphore, swapchain, imageIndex);
    //    }

    //    private void UpdateUniformBuffer(float deltaTime)
    //    {
    //        UniformBufferObject ubo = new UniformBufferObject();
    //        ubo.model = Matrix4x4.CreateFromAxisAngle(new Vector3(0.0f, 0.0f, 1.0f), deltaTime * Radian(90));

    //        ubo.view = Matrix4x4.CreateLookAt(new Vector3(2.0f, 2.0f, 4.0f), new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 0.0f, 1.0f));

    //        //TODO: Use the size of the swapchain
    //        ubo.proj = Matrix4x4.CreatePerspectiveFieldOfView(Radian(45.0f), cryoApp.Host.window.Width / cryoApp.Host.window.Height, 0.1f, 10.0f);
    //        ubo.proj.M22 *= -1;



    //        IntPtr ptr = uniformBuffer.Memory.Map(0, Interop.SizeOf<UniformBufferObject>());
    //        Interop.Write(ptr, ref ubo);
    //        uniformBuffer.Memory.Unmap();
    //    }

    //    private void ToDispose<T>(T disposable)
    //    {
    //        switch (disposable)
    //        {
    //            case IEnumerable<IDisposable> sequence:
    //                foreach (var element in sequence)
    //                    _toDisposeFrame.Push(element);
    //                break;
    //            case IDisposable element:
    //                _toDisposeFrame.Push(element);
    //                break;
    //        }
    //    }



    //}

    [StructLayout(LayoutKind.Sequential)]
    struct UniformBufferObject
    {
        public Matrix4x4 model;
        public Matrix4x4 view;
        public Matrix4x4 proj;
    };
}
