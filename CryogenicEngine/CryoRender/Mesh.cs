﻿using System.Numerics;
using System.Runtime.InteropServices;
using VulkanCore;

namespace CryogenicEngine.CryoRender
{
    public class Mesh
    {
        public Vertex[] vertices;
        public int[] indeces;

        internal CryoVulkan.CryoMannagedBuffer vertexBuffer;
        internal CryoVulkan.CryoMannagedBuffer indexBuffer;

        internal bool copyed;

        public Mesh(Vertex[] vertices, int[] indeces)
        {
            this.vertices = vertices;
            this.indeces = indeces;
        }

        public void CopyToMemory()
        {
            if (copyed)
            {
                return;
            }
            long size = vertices.Length * Interop.SizeOf<Vertex>();
            vertexBuffer = CryoVulkan.CryoVulkanBufferManagger.Instance.CreateBuffer(BufferUsages.VertexBuffer, size, vertices);

            size = indeces.Length * Interop.SizeOf<int>();
            indexBuffer = CryoVulkan.CryoVulkanBufferManagger.Instance.CreateBuffer(BufferUsages.IndexBuffer, size, indeces);


            copyed = true;
        }


        public static Mesh Box(float width, float height, float depth)
        {
            float w2 = 0.5f * width;
            float h2 = 0.5f * height;
            float d2 = 0.5f * depth;

            Vertex[] vertices =
            {
                // Fill in the front face vertex data.
                new Vertex(-w2, +h2, -d2, +0, +0, -1, +0, +0),
                new Vertex(-w2, -h2, -d2, +0, +0, -1, +0, +1),
                new Vertex(+w2, -h2, -d2, +0, +0, -1, +1, +1),
                new Vertex(+w2, +h2, -d2, +0, +0, -1, +1, +0),
                // Fill in the back face vertex data.
                new Vertex(-w2, +h2, +d2, +0, +0, +1, +1, +0),
                new Vertex(+w2, +h2, +d2, +0, +0, +1, +0, +0),
                new Vertex(+w2, -h2, +d2, +0, +0, +1, +0, +1),
                new Vertex(-w2, -h2, +d2, +0, +0, +1, +1, +1),
                // Fill in the top face vertex data.
                new Vertex(-w2, -h2, -d2, +0, +1, +0, +0, +0),
                new Vertex(-w2, -h2, +d2, +0, +1, +0, +0, +1),
                new Vertex(+w2, -h2, +d2, +0, +1, +0, +1, +1),
                new Vertex(+w2, -h2, -d2, +0, +1, +0, +1, +0),
                // Fill in the bottom face vertex data.
                new Vertex(-w2, +h2, -d2, +0, -1, +0, +1, +0),
                new Vertex(+w2, +h2, -d2, +0, -1, +0, +0, +0),
                new Vertex(+w2, +h2, +d2, +0, -1, +0, +0, +1),
                new Vertex(-w2, +h2, +d2, +0, -1, +0, +1, +1),
                // Fill in the left face vertex data.
                new Vertex(-w2, +h2, +d2, -1, +0, +0, +0, +0),
                new Vertex(-w2, -h2, +d2, -1, +0, +0, +0, +1),
                new Vertex(-w2, -h2, -d2, -1, +0, +0, +1, +1),
                new Vertex(-w2, +h2, -d2, -1, +0, +0, +1, +0),
                // Fill in the right face vertex data.
                new Vertex(+w2, +h2, -d2, +1, +0, +0, +0, +0),
                new Vertex(+w2, -h2, -d2, +1, +0, +0, +0, +1),
                new Vertex(+w2, -h2, +d2, +1, +0, +0, +1, +1),
                new Vertex(+w2, +h2, +d2, +1, +0, +0, +1, +0)
            };

            int[] indices =
            {
                // Fill in the front face index data.
                0, 1, 2, 0, 2, 3,
                // Fill in the back face index data.
                4, 5, 6, 4, 6, 7,
                // Fill in the top face index data.
                8, 9, 10, 8, 10, 11,
                // Fill in the bottom face index data.
                12, 13, 14, 12, 14, 15,
                // Fill in the left face index data
                16, 17, 18, 16, 18, 19,
                // Fill in the right face index data
                20, 21, 22, 20, 22, 23
            };

            return new Mesh(vertices, indices);
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Vertex
    {
        public CryoMath.Vector3 Position;
        public CryoMath.Vector3 Color;
        public Vector2 UV;

        public Vertex(
            float px, float py, float pz,
            float cr, float cg, float cb,
            float tu, float tv
            )
        {
            Position = new CryoMath.Vector3(px, py, pz);
            //Normal = new Vector3(0, 0, 0);
            Color = new CryoMath.Vector3(cr, cg, cb);
            UV = new Vector2(tu, tv);
        }

        public Vertex(CryoMath.Vector3 position, CryoMath.Vector3 color, Vector2 uv)
        {
            Position = position;
            Color = color;
            UV = uv;
        }

        public static VertexInputBindingDescription[] getVertexInputBindingDescription()
        {
            return new[] { new VertexInputBindingDescription(0, Interop.SizeOf<Vertex>(), VertexInputRate.Vertex) };
        }

        public static VertexInputAttributeDescription[] getVertexInputAttributeDescription()
        {
            return new[]
                    {
                    new VertexInputAttributeDescription(0, 0, Format.R32G32B32SFloat, Marshal.OffsetOf<Vertex>("Position").ToInt32()),  // Position.
                    new VertexInputAttributeDescription(1, 0, Format.R32G32B32SFloat, Marshal.OffsetOf<Vertex>("Color").ToInt32()), // Color.
                    new VertexInputAttributeDescription(2, 0, Format.R32G32SFloat, Marshal.OffsetOf<Vertex>("UV").ToInt32())
                    };
        }

        public override string ToString()
        {
            //TODO: write out all the parameters
            return "Pos: " + Position.X + "," + Position.Y + "," + Position.Z;
        }

    }

}