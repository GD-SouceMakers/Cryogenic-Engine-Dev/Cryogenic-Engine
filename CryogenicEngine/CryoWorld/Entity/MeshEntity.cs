﻿using CryogenicEngine.CryoMath;
using CryogenicEngine.CryoRender;

namespace CryogenicEngine.CryoWorld
{
    public class MeshEntity : Entity
    {
        public MeshEntity(Component parent, Transform t, Mesh mesh):base(parent,t)
        {
            internalHierarchy.Add(new MeshRenderer(this,mesh));
        }
    }
}