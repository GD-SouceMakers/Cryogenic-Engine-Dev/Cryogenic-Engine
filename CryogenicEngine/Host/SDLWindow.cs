﻿using SDL2;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace CryogenicEngine.Host
{
    public class SDLWindow : IAppWindow
    {
        SDLContext sdlContext;

        private readonly string _title;
        private IntPtr _window;

        private bool _minimized; // Is the application minimized?
        private bool _maximized; // Is the application maximized?
        private bool _resizing;  // Are the resize bars being dragged?



        public IntPtr WindowHandle => GetWindowHandle().info.win.window;
        public IntPtr WindowHInstance => Process.GetCurrentProcess().Handle;
        public SDL.SDL_SysWMinfo GetWindowHandle()
        {
            SDL.SDL_SysWMinfo wMinfo = new SDL.SDL_SysWMinfo();
            SDL.SDL_GetWindowWMInfo(_window, ref wMinfo);
            return wMinfo;
        }


        public SDLWindow(SDLContext context)
        {
            sdlContext = context;
        }


        public int Width { get; private set; } = 300;
        public int Height { get; private set; } = 200;

        public void Initialize()
        {
            _window = SDL.SDL_CreateWindow("test", 300, 100, Width, Height, SDL.SDL_WindowFlags.SDL_WINDOW_VULKAN);
            

            #region OLD FORM
            /*
            _form = new Form
            {
                Text = _title,
                FormBorderStyle = FormBorderStyle.Sizable,
                ClientSize = new System.Drawing.Size(Width, Height),
                StartPosition = FormStartPosition.CenterScreen,
                MinimumSize = new System.Drawing.Size(200, 200),
                Visible = false
            };
            _form.ResizeBegin += (sender, e) =>
            {
                _appPaused = true;
                _resizing = true;
                _timer.Stop();
            };
            _form.ResizeEnd += (sender, e) =>
            {
                _appPaused = false;
                _resizing = false;
                _timer.Start();
                _app.Resize();
            };
            _form.Activated += (sender, e) =>
            {
                _appPaused = false;
                _timer.Start();
            };
            _form.Deactivate += (sender, e) =>
            {
                _appPaused = true;
                _timer.Stop();
            };
            _form.HandleDestroyed += (sender, e) => _running = false;
            _form.Resize += (sender, e) =>
            {
                Width = _form.ClientSize.Width;
                Height = _form.ClientSize.Height;
                    // When window state changes.
                    if (_form.WindowState != _lastWindowState)
                {
                    _lastWindowState = _form.WindowState;
                    if (_form.WindowState == FormWindowState.Maximized)
                    {
                        _appPaused = false;
                        _minimized = false;
                        _maximized = true;
                        _app.Resize();
                    }
                    else if (_form.WindowState == FormWindowState.Minimized)
                    {
                        _appPaused = true;
                        _minimized = true;
                        _maximized = false;
                    }
                    else if (_form.WindowState == FormWindowState.Normal)
                    {
                        if (_minimized) // Restoring from minimized state?
                            {
                            _appPaused = false;
                            _minimized = false;
                            _app.Resize();
                        }
                        else if (_maximized) // Restoring from maximized state?
                            {
                            _appPaused = false;
                            _maximized = false;
                            _app.Resize();
                        }
                        else if (_resizing)
                        {
                                // If user is dragging the resize bars, we do not resize 
                                // the buffers here because as the user continuously 
                                // drags the resize bars, a stream of WM_SIZE messages are
                                // sent to the window, and it would be pointless (and slow)
                                // to resize for each WM_SIZE message received from dragging
                                // the resize bars. So instead, we reset after the user is 
                                // done resizing the window and releases the resize bars, which 
                                // sends a WM_EXITSIZEMOVE message.
                            }
                        else // API call such as SetWindowPos or setting fullscreen state.
                            {
                            _app.Resize();
                        }
                    }
                }
                else if (!_resizing) // Resize due to snapping.
                    {
                    _app.Resize();
                }
            };
            */
            #endregion
        }

        public IntPtr GetWindowPointer()
        {
            return _window;
        }


        public void Dispose()
        {
            SDL.SDL_Quit();
        }





    }
}

