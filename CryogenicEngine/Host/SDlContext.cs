﻿using SDL2;
using System;
using System.Collections.Generic;
using System.Text;

namespace CryogenicEngine.Host
{
    public class SDLContext : IAppHost
    {
        public SDLWindow window;
        public SDL.SDL_Event eventInfo;

        public SDLContext()
        {
        }

        public void Initialize()
        {
            SDL.SDL_Init(SDL.SDL_INIT_EVERYTHING);
        }

        public SDLWindow CreateWindow()
        {
            window = new SDLWindow(this);

            eventInfo = new SDL.SDL_Event();

            return window;
        }

        public void PostWindowCreate()
        {
            //SDL2.SDL.SDL_SetRelativeMouseMode(SDL.SDL_bool.SDL_TRUE);
            //SDL.SDL_CaptureMouse(SDL.SDL_bool.SDL_TRUE);
        }

        public bool CheckForSDLErrors()
        {
            string error = SDL.SDL_GetError();

            if (!String.IsNullOrEmpty(error))
            {
                Console.WriteLine("SDL ERROR: {0}", error);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
